package com.aplicacion.aplicacion.service;

import com.aplicacion.aplicacion.entidades.Producto;
import com.aplicacion.aplicacion.service.dto.ProductoDto;
import org.springframework.stereotype.Service;
import java.util.List;
import org.springframework.http.ResponseEntity;

@Service
public interface ProductoService {
    List<ProductoDto> finAll();
    ProductoDto findById(Integer id);
    ProductoDto create (ProductoDto productoDto);
    ProductoDto update (ProductoDto productoDto);
    public void delete(Integer id);
    public ResponseEntity buscar (String nombre, String codigo);
    public Iterable<Producto> buscarCodigoProducto(String codigo);
}
