package com.aplicacion.aplicacion.service.dto;

import com.aplicacion.aplicacion.entidades.Producto;
import com.aplicacion.aplicacion.entidades.Usuarios;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class CompraDto implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;
        
    private String cantidad;
    
    private LocalDateTime  fecha;
    
    private Usuarios usuarios;
    
    private Producto producto;
}
