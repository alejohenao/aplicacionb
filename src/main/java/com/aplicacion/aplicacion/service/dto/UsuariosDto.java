package com.aplicacion.aplicacion.service.dto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Setter
@Getter
public class UsuariosDto {
        @Id
        private int id;

        private String documento;
        private String nombre;
        private String apellido;
        private String direccion;
        private String telefono;

}
