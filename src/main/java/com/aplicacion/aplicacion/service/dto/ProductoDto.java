package com.aplicacion.aplicacion.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
public class ProductoDto implements Serializable {

        @Id
        private int id;
        private String codigo;
        private String nombre;
        private String precio;
        private String stock;



}
