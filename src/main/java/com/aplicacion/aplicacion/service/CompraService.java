package com.aplicacion.aplicacion.service;
import com.aplicacion.aplicacion.service.dto.CompraDto;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface CompraService {
    CompraDto create(CompraDto compraDto);
    List<CompraDto> findAll();
}

