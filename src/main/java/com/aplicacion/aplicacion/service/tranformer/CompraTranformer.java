package com.aplicacion.aplicacion.service.tranformer;

import com.aplicacion.aplicacion.entidades.Compra;
import com.aplicacion.aplicacion.service.dto.CompraDto;


public class CompraTranformer {
public static Compra getCompraByCompraDto(CompraDto dto){
if (dto==null){
return null;
}
Compra compra = new Compra();
compra.setId(dto.getId());
compra.setCantidad(dto.getCantidad());
compra.setFecha(dto.getFecha());
compra.setUsuarios(dto.getUsuarios());
compra.setProducto(dto.getProducto());
return compra;
}
public static CompraDto getCompraDtoByCompra(Compra compra){
if (compra==null){
return null;
}
CompraDto dto = new CompraDto();
dto.setId(compra.getId());
dto.setCantidad(compra.getCantidad());
dto.setFecha(compra.getFecha());
dto.setUsuarios(compra.getUsuarios());
dto.setProducto(compra.getProducto());
return dto;
}
}

