package com.aplicacion.aplicacion.service.tranformer;

import com.aplicacion.aplicacion.entidades.Usuarios;
import com.aplicacion.aplicacion.service.dto.UsuariosDto;

public class UsuariosTranformer {
    public static Usuarios getUsuariosByUsuariosDto(UsuariosDto dto){
        if (dto==null){
            return null;
        }
        Usuarios usuarios = new Usuarios();
        usuarios.setId(dto.getId());
        usuarios.setDocumento(dto.getDocumento());
        usuarios.setNombre(dto.getNombre());
        usuarios.setApellido(dto.getApellido());
        usuarios.setDireccion(dto.getDireccion());
        usuarios.setTelefono(dto.getTelefono());
        return usuarios;
    }
    public static UsuariosDto getUsuariosDtoByUsuarios(Usuarios usuarios){
        if (usuarios ==null){
            return null;
        }
        UsuariosDto dto = new UsuariosDto();
        dto.setId(usuarios.getId());
        dto.setDocumento(usuarios.getDocumento());
        dto.setNombre(usuarios.getNombre());
        dto.setApellido(usuarios.getApellido());
        dto.setDireccion(usuarios.getDireccion());
        dto.setTelefono(usuarios.getTelefono());
        return dto;
    }
}
