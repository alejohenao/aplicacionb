package com.aplicacion.aplicacion.service.tranformer;

import com.aplicacion.aplicacion.entidades.Producto;
import com.aplicacion.aplicacion.service.dto.ProductoDto;

public class ProductoTranformer {
    public static Producto getProductoByProductoDto(ProductoDto dto){
        if (dto==null){
            return null;
        }
        Producto producto = new Producto();
        producto.setId(dto.getId());
        producto.setCodigo(dto.getCodigo());
        producto.setNombre(dto.getNombre());
        producto.setPrecio(dto.getPrecio());
        producto.setStock(dto.getStock());
        return producto;
    }
    public static ProductoDto getProductoDtoByProducto(Producto producto){
        if (producto ==null){
            return null;
        }
        ProductoDto dto = new ProductoDto();
        dto.setId(producto.getId());
        dto.setCodigo(producto.getCodigo());
        dto.setNombre(producto.getNombre());
        dto.setPrecio(producto.getPrecio());
        dto.setStock(producto.getStock());
        return dto;
    }
}
