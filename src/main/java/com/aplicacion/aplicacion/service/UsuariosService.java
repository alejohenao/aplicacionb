package com.aplicacion.aplicacion.service;

import com.aplicacion.aplicacion.entidades.Usuarios;
import com.aplicacion.aplicacion.service.dto.UsuariosDto;
import org.springframework.stereotype.Service;
import java.util.List;
import org.springframework.http.ResponseEntity;

@Service
public interface UsuariosService {
    List<UsuariosDto> findAll();
    UsuariosDto findById(Integer id);
    UsuariosDto create (UsuariosDto usuariosDto);
    UsuariosDto update(UsuariosDto usuariosDto);
    public void delete(Integer id);
    public ResponseEntity buscar (String nombre, String documento);
    public Iterable<Usuarios> buscarDocumentoUsuario (String documento);
    
    
}
