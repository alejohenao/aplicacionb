package com.aplicacion.aplicacion.service.imp;

import com.aplicacion.aplicacion.entidades.Producto;
import com.aplicacion.aplicacion.repository.ProductoRepository;
import com.aplicacion.aplicacion.service.ProductoService;
import com.aplicacion.aplicacion.service.dto.ProductoDto;
import com.aplicacion.aplicacion.service.tranformer.ProductoTranformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Service
public class ProductoImp implements ProductoService {

    @Autowired
    ProductoRepository productoRepository;

    @Override
    public List<ProductoDto> finAll() {
        return productoRepository.findAll().stream().map(ProductoTranformer::getProductoDtoByProducto)
                .collect(Collectors.toList());
    }

    @Override
    public ProductoDto findById(Integer id){
        Optional<Producto> optional = productoRepository.findById(id);
        if (optional.isPresent()){
            return ProductoTranformer.getProductoDtoByProducto(optional.get());
        }
        return null;
    }
    @Override
    public ProductoDto create(ProductoDto productoDto){
        Producto producto = ProductoTranformer.getProductoByProductoDto(productoDto);
        return ProductoTranformer.getProductoDtoByProducto(productoRepository.save(producto));
    }

    @Override
    public ProductoDto update(ProductoDto productoDto){
        Producto producto = ProductoTranformer.getProductoByProductoDto(productoDto);
        return ProductoTranformer.getProductoDtoByProducto(productoRepository.save(producto));
    }
    @Override
    public void delete(Integer id) {productoRepository.deleteById(id);}
    
    @Override
    public Iterable<Producto> buscarCodigoProducto(String codigo){
      return productoRepository.findByCodigoContaining(codigo);
    }
    @Override
    public ResponseEntity buscar (String nombre, String codigo){
    if (nombre != null && codigo != null){
    return new ResponseEntity(productoRepository.findByNombreContainsAndCodigoContains(nombre, codigo), HttpStatus.OK);
    } else if (nombre != null){
    return new ResponseEntity(productoRepository.findByNombreContains(nombre), HttpStatus.OK);
    } else if (codigo != null){
    return new ResponseEntity(productoRepository.findByCodigoContains(codigo),HttpStatus.OK);
    } else{
    return new ResponseEntity("No hay ningun dato para poder consultar", HttpStatus.BAD_REQUEST);
     }
    }
}
