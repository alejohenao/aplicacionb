
package com.aplicacion.aplicacion.service.imp;

import com.aplicacion.aplicacion.entidades.Compra;
import com.aplicacion.aplicacion.entidades.Producto;
import com.aplicacion.aplicacion.entidades.Usuarios;
import com.aplicacion.aplicacion.repository.CompraRepository;
import com.aplicacion.aplicacion.repository.ProductoRepository;
import com.aplicacion.aplicacion.repository.UsuariosRepository;
import com.aplicacion.aplicacion.service.CompraService;
import com.aplicacion.aplicacion.service.dto.CompraDto;
import com.aplicacion.aplicacion.service.tranformer.CompraTranformer;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CompraImp implements CompraService {

    @Autowired
    CompraRepository compraRepository;
    //se llama los repository para consulta del producto y usuario
    @Autowired
    ProductoRepository productoRepository;
    @Autowired
    UsuariosRepository usuarioRepository;
    
    @Override
    public CompraDto create(CompraDto compraDto) {
       //se llama los repository para consulta del producto y usuario
       Optional<Producto> producto = productoRepository.findByCodigo(compraDto.getProducto().getCodigo());
       Optional<Usuarios> usuario = usuarioRepository.findByDocumento(compraDto.getUsuarios().getDocumento());
       //
       Compra compra = CompraTranformer.getCompraByCompraDto(compraDto);
        //se llama los repository para consulta del producto y usuario
       compra.setProducto(producto.get());
       compra.setUsuarios(usuario.get());
       //
       return CompraTranformer.getCompraDtoByCompra(compraRepository.save(compra));
    }

    @Override
    public List <CompraDto> findAll(){
    return compraRepository.findAll().stream().map(CompraTranformer::getCompraDtoByCompra)
            .collect(Collectors.toList());}
    
}
