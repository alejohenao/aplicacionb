package com.aplicacion.aplicacion.service.imp;

import com.aplicacion.aplicacion.entidades.Usuarios;
import com.aplicacion.aplicacion.repository.UsuariosRepository;
import com.aplicacion.aplicacion.service.UsuariosService;
import com.aplicacion.aplicacion.service.dto.UsuariosDto;
import com.aplicacion.aplicacion.service.tranformer.UsuariosTranformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Service
public class UsuariosImp implements UsuariosService {

    @Autowired
    UsuariosRepository usuariosRepository;

    @Override
    public List<UsuariosDto> findAll() {
        return usuariosRepository.findAll().stream().map(UsuariosTranformer::getUsuariosDtoByUsuarios)
                .collect(Collectors.toList());
    }

    @Override
    public UsuariosDto findById(Integer id){
        Optional<Usuarios> optional = usuariosRepository.findById(id);
        if (optional.isPresent()){
            return UsuariosTranformer.getUsuariosDtoByUsuarios(optional.get());
        }
        return null;
    }
    @Override
    public UsuariosDto create(UsuariosDto usuariosDto){
        Usuarios usuarios = UsuariosTranformer.getUsuariosByUsuariosDto(usuariosDto);
        return UsuariosTranformer.getUsuariosDtoByUsuarios(usuariosRepository.save(usuarios));
    }

    @Override
    public UsuariosDto update(UsuariosDto usuariosDto){
        Usuarios usuarios = UsuariosTranformer.getUsuariosByUsuariosDto(usuariosDto);
        return UsuariosTranformer.getUsuariosDtoByUsuarios(usuariosRepository.save(usuarios));
    }
    @Override
    public void delete(Integer id) {usuariosRepository.deleteById(id);}
    
    @Override
    public Iterable<Usuarios> buscarDocumentoUsuario(String documento){
        return usuariosRepository.findByDocumentoContaining(documento);
    }
    
    @Override
    public ResponseEntity buscar(String nombre, String documento) {
        if (nombre != null && documento != null){
            return new ResponseEntity(usuariosRepository.findByNombreContainsAndDocumentoContains(nombre, documento), HttpStatus.OK);

        }else if (nombre != null){
            return new ResponseEntity(usuariosRepository.findByNombreContains(nombre), HttpStatus.OK);
        }else if (documento !=null){
            return new ResponseEntity(usuariosRepository.findByDocumentoContains(documento), HttpStatus.OK);
        }else{
            return new ResponseEntity("No ha datos suficientes para la consulta", HttpStatus.BAD_REQUEST);
        }
    }

}
