package com.aplicacion.aplicacion.entidades;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
public class Usuarios {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;

    private String documento;
    private String nombre;
    private String apellido;
    private String direccion;
    private String telefono;

}
