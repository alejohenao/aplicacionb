package com.aplicacion.aplicacion.repository;

import com.aplicacion.aplicacion.entidades.Usuarios;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuariosRepository extends JpaRepository<Usuarios, Integer> {
    
    Optional<Usuarios> findByDocumento(String documento);

    Iterable<Usuarios> findByNombreContains(String nombre);

    Iterable<Usuarios> findByDocumentoContains(String documento);

    Iterable<Usuarios> findByNombreContainsAndDocumentoContains(String name, String documento);

    Iterable<Usuarios> findByDocumentoContaining (String documento);
  
}

