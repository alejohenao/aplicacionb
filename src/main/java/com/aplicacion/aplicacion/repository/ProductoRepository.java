package com.aplicacion.aplicacion.repository;

import com.aplicacion.aplicacion.entidades.Producto;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductoRepository extends JpaRepository<Producto, Integer>{
    Optional<Producto> findByCodigo(String codigo);
    Iterable<Producto> findByCodigoContaining(String codigo);
    Iterable<Producto> findByCodigoContains(String codigo);
    Iterable<Producto> findByNombreContains(String nombre);
    Iterable<Producto> findByNombreContainsAndCodigoContains(String nombre, String codigo);
}
