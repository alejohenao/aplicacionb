
package com.aplicacion.aplicacion.repository;

import com.aplicacion.aplicacion.entidades.Compra;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CompraRepository extends JpaRepository<Compra, Integer>{

  
}
