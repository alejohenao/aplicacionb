package com.aplicacion.aplicacion.webrest;

import com.aplicacion.aplicacion.service.CompraService;
import com.aplicacion.aplicacion.service.dto.CompraDto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/compra")
public class CompraResource {

 @Autowired
 CompraService compraService;
 
 
@PostMapping("/")
    public CompraDto create(@RequestBody CompraDto compraDto){return compraService.create(compraDto);}

@GetMapping("/")
     public ResponseEntity<List<CompraDto>>findAll(){
       return new ResponseEntity<>(compraService.findAll(),HttpStatus.OK);}
}