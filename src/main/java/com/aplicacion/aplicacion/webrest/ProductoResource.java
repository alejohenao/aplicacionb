package com.aplicacion.aplicacion.webrest;

import com.aplicacion.aplicacion.entidades.Producto;
import com.aplicacion.aplicacion.service.ProductoService;
import com.aplicacion.aplicacion.service.dto.ProductoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/producto")
public class ProductoResource {

    @Autowired
    ProductoService productoService;

    @GetMapping("")
    public ResponseEntity<List<ProductoDto>> findAll(){
        return new ResponseEntity<>(productoService.finAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductoDto> findById(@PathVariable Integer id){
        return new ResponseEntity<>(productoService.findById(id),HttpStatus.OK);
    }
    @PostMapping("")
    public ProductoDto create(@RequestBody ProductoDto productoDto){return productoService.create(productoDto);
    }
    @PutMapping("")
    public ProductoDto update(@RequestBody ProductoDto productoDto){return productoService.update(productoDto);
    }
    @DeleteMapping("{id}")
    public void delete(@RequestBody @PathVariable Integer id){productoService.delete(id);
    }
    @GetMapping("/buscar")
    public ResponseEntity buscar(@RequestParam(value = "nombre",required = false)String nombre,
                                 @RequestParam(value ="codigo", required = false)String codigo){
    return productoService.buscar(nombre, codigo);
    }
    //buscar por codigo
    @GetMapping("/buscarCodigoProducto")
    public Iterable <Producto> buscarCodigoProducto (@RequestParam(value = "codigo", required = false)String codigo){
     return productoService.buscarCodigoProducto(codigo);
    }
}
