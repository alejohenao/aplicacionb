package com.aplicacion.aplicacion.webrest;

import com.aplicacion.aplicacion.entidades.Usuarios;
import com.aplicacion.aplicacion.service.UsuariosService;
import com.aplicacion.aplicacion.service.dto.UsuariosDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/usuarios")
public class UsuariosResource {

    @Autowired
    UsuariosService usuariosService;
    //listar todos
    @GetMapping("")
    public ResponseEntity<List<UsuariosDto>> findAll(){
        return  new ResponseEntity<>(usuariosService.findAll(), HttpStatus.OK);
    }

    //listar por id
    @GetMapping("/{id}")
    public ResponseEntity<UsuariosDto> findById(@PathVariable Integer id){
        return new ResponseEntity<>(usuariosService.findById(id),HttpStatus.OK);

    }
    // crear
    @PostMapping("")
    public UsuariosDto create(@RequestBody UsuariosDto usuariosDto) {
        return usuariosService.create(usuariosDto);
    }

    //actualizar

    @PutMapping("")
    public UsuariosDto update(@RequestBody  UsuariosDto usuariosDto) {return usuariosService.update(usuariosDto);
    }
    // eliminar
    @DeleteMapping("/{id}")
    public void delete(@RequestBody @PathVariable Integer id) {usuariosService.delete(id);
    }

    //buscar
    @GetMapping("/buscar")
    public ResponseEntity buscar(@RequestParam(value = "nombre", required = false)String nombre,
                              @RequestParam(value = "documento", required = false)String documento){
    return usuariosService.buscar(nombre,documento);
            }
    //buscar por documento
    @GetMapping("/buscarDocumentoUsuario")
    public Iterable <Usuarios> buscarDocumentoUsuario (@RequestParam(value = "documento", required = false) String documento){
           return usuariosService.buscarDocumentoUsuario(documento);
    }
   
}
